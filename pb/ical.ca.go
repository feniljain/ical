package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	IcalsGenerateIcalContentActivity = "/saastack.ical.v1.Icals/GenerateIcalContent"
)

func RegisterIcalsActivities(cli IcalsClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GenerateIcalContentRequest) (*GenerateIcalContentResponse, error) {
			res, err := cli.GenerateIcalContent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: IcalsGenerateIcalContentActivity},
	)
}

// IcalsActivitiesClient is a typesafe wrapper for IcalsActivities.
type IcalsActivitiesClient struct {
}

// NewIcalsActivitiesClient creates a new IcalsActivitiesClient.
func NewIcalsActivitiesClient(cli IcalsClient) IcalsActivitiesClient {
	RegisterIcalsActivities(cli)
	return IcalsActivitiesClient{}
}

func (ca *IcalsActivitiesClient) GenerateIcalContent(ctx workflow.Context, in *GenerateIcalContentRequest) (*GenerateIcalContentResponse, error) {
	future := workflow.ExecuteActivity(ctx, IcalsGenerateIcalContentActivity, in)
	var result GenerateIcalContentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
