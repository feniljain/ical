// Code generated by protoc-gen-nakaab, DO NOT EDIT.

package pb

import (
	"go.saastack.io/protoc-gen-nakaab/nakaab"
)

func GenerateIcalContentRequestFields(ff ...IsGenerateIcalContentRequestField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func GenerateIcalContentRequestAllFields() []string {

	return []string{
		// primitive fields
		"id",
		// embedded fields
	}
}

type IsGenerateIcalContentRequestField interface {
	nakaab.Unwrapper
	isGenerateIcalContentRequestField()
}

type generateIcalContentRequestPrimitiveField string

const (
	GenerateIcalContentRequest_Id generateIcalContentRequestPrimitiveField = "id"
)

func (s generateIcalContentRequestPrimitiveField) isGenerateIcalContentRequestField() {}

func (s generateIcalContentRequestPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

func GenerateIcalContentResponseFields(ff ...IsGenerateIcalContentResponseField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func GenerateIcalContentResponseAllFields() []string {

	return []string{
		// primitive fields
		"content",
		// embedded fields
	}
}

type IsGenerateIcalContentResponseField interface {
	nakaab.Unwrapper
	isGenerateIcalContentResponseField()
}

type generateIcalContentResponsePrimitiveField string

const (
	GenerateIcalContentResponse_Content generateIcalContentResponsePrimitiveField = "content"
)

func (s generateIcalContentResponsePrimitiveField) isGenerateIcalContentResponseField() {}

func (s generateIcalContentResponsePrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}
