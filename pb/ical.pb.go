// Code generated by protoc-gen-go. DO NOT EDIT.
// source: ical.proto

package pb

import (
	context "context"
	fmt "fmt"
	math "math"

	_ "github.com/Shivam010/protoc-gen-validate/validate"
	proto "github.com/golang/protobuf/proto"
	_ "go.saastack.io/chaku/validate"
	_ "go.saastack.io/events/eventspush"
	_ "go.saastack.io/jaal/schema"
	_ "go.saastack.io/pehredaar/pehredaar"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type GenerateIcalContentRequest struct {
	//Can be employee or location id
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GenerateIcalContentRequest) Reset()         { *m = GenerateIcalContentRequest{} }
func (m *GenerateIcalContentRequest) String() string { return proto.CompactTextString(m) }
func (*GenerateIcalContentRequest) ProtoMessage()    {}
func (*GenerateIcalContentRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8bacaa7819309039, []int{0}
}

func (m *GenerateIcalContentRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GenerateIcalContentRequest.Unmarshal(m, b)
}
func (m *GenerateIcalContentRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GenerateIcalContentRequest.Marshal(b, m, deterministic)
}
func (m *GenerateIcalContentRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GenerateIcalContentRequest.Merge(m, src)
}
func (m *GenerateIcalContentRequest) XXX_Size() int {
	return xxx_messageInfo_GenerateIcalContentRequest.Size(m)
}
func (m *GenerateIcalContentRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GenerateIcalContentRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GenerateIcalContentRequest proto.InternalMessageInfo

func (m *GenerateIcalContentRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type GenerateIcalContentResponse struct {
	//content output as string temporarily, can be changed to streamed bytes for file output
	Content              string   `protobuf:"bytes,1,opt,name=content,proto3" json:"content,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GenerateIcalContentResponse) Reset()         { *m = GenerateIcalContentResponse{} }
func (m *GenerateIcalContentResponse) String() string { return proto.CompactTextString(m) }
func (*GenerateIcalContentResponse) ProtoMessage()    {}
func (*GenerateIcalContentResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_8bacaa7819309039, []int{1}
}

func (m *GenerateIcalContentResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GenerateIcalContentResponse.Unmarshal(m, b)
}
func (m *GenerateIcalContentResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GenerateIcalContentResponse.Marshal(b, m, deterministic)
}
func (m *GenerateIcalContentResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GenerateIcalContentResponse.Merge(m, src)
}
func (m *GenerateIcalContentResponse) XXX_Size() int {
	return xxx_messageInfo_GenerateIcalContentResponse.Size(m)
}
func (m *GenerateIcalContentResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GenerateIcalContentResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GenerateIcalContentResponse proto.InternalMessageInfo

func (m *GenerateIcalContentResponse) GetContent() string {
	if m != nil {
		return m.Content
	}
	return ""
}

func init() {
	proto.RegisterType((*GenerateIcalContentRequest)(nil), "saastack.ical.v1.GenerateIcalContentRequest")
	proto.RegisterType((*GenerateIcalContentResponse)(nil), "saastack.ical.v1.GenerateIcalContentResponse")
}

func init() { proto.RegisterFile("ical.proto", fileDescriptor_8bacaa7819309039) }

var fileDescriptor_8bacaa7819309039 = []byte{
	// 296 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xca, 0x4c, 0x4e, 0xcc,
	0xd1, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x12, 0x28, 0x4e, 0x4c, 0x2c, 0x2e, 0x49, 0x4c, 0xce,
	0xd6, 0x03, 0x0b, 0x96, 0x19, 0x4a, 0xc9, 0x26, 0xe6, 0xe5, 0xe5, 0x97, 0x24, 0x96, 0x64, 0xe6,
	0xe7, 0x15, 0xeb, 0x23, 0xb1, 0x21, 0x1a, 0xa4, 0xc4, 0xcb, 0x12, 0x73, 0x32, 0x53, 0x12, 0x4b,
	0x52, 0xf5, 0x61, 0x0c, 0xa8, 0x84, 0x08, 0x5c, 0x22, 0x39, 0x23, 0x31, 0xbb, 0x14, 0x2a, 0x2a,
	0x59, 0x90, 0x9a, 0x51, 0x94, 0x9a, 0x92, 0x98, 0x58, 0xa4, 0x0f, 0x67, 0x41, 0xa5, 0x44, 0x53,
	0xcb, 0x52, 0xf3, 0x4a, 0x8a, 0x0b, 0x4a, 0x8b, 0x33, 0xf4, 0x41, 0x04, 0x54, 0x58, 0xb8, 0x38,
	0x39, 0x23, 0x35, 0x37, 0x51, 0x1f, 0x42, 0x41, 0x04, 0x95, 0xcc, 0xb9, 0xa4, 0xdc, 0x53, 0xf3,
	0x52, 0x8b, 0x12, 0x4b, 0x52, 0x3d, 0x93, 0x13, 0x73, 0x9c, 0xf3, 0xf3, 0x4a, 0x52, 0xf3, 0x4a,
	0x82, 0x52, 0x0b, 0x4b, 0x53, 0x8b, 0x4b, 0x84, 0x24, 0xb9, 0x98, 0x32, 0x53, 0x24, 0x18, 0x15,
	0x18, 0x35, 0x38, 0x9d, 0x38, 0x67, 0x3d, 0x3e, 0xc0, 0xcc, 0x52, 0xc4, 0x24, 0xc0, 0x1c, 0xc4,
	0x94, 0x99, 0xa2, 0x64, 0xce, 0x25, 0x8d, 0x55, 0x63, 0x71, 0x41, 0x7e, 0x5e, 0x71, 0xaa, 0x90,
	0x04, 0x17, 0x7b, 0x32, 0x44, 0x08, 0xa2, 0x3d, 0x08, 0xc6, 0x35, 0x5a, 0xc6, 0xc8, 0xc5, 0x0a,
	0xd2, 0x51, 0x2c, 0x34, 0x87, 0x91, 0x4b, 0x18, 0x8b, 0x19, 0x42, 0x3a, 0x7a, 0xe8, 0x61, 0xa7,
	0x87, 0xdb, 0x8d, 0x52, 0xba, 0x44, 0xaa, 0x86, 0x38, 0x4c, 0x49, 0x63, 0xd6, 0x3b, 0x16, 0x16,
	0x90, 0xb7, 0x76, 0xed, 0xd3, 0x61, 0xe3, 0x62, 0x01, 0x69, 0x69, 0xba, 0xfc, 0x64, 0x32, 0x93,
	0x80, 0x10, 0x9f, 0x7e, 0x99, 0xa1, 0x3e, 0x88, 0x5f, 0xac, 0x5f, 0x9d, 0x99, 0x52, 0xeb, 0xc4,
	0x12, 0xc5, 0x54, 0x90, 0x94, 0xc4, 0x06, 0x0e, 0x27, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff,
	0x86, 0x4e, 0xa3, 0x25, 0xdc, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// IcalsClient is the client API for Icals service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type IcalsClient interface {
	// GenerateIcalContent generates ical content from id(can be employee or location id)
	GenerateIcalContent(ctx context.Context, in *GenerateIcalContentRequest, opts ...grpc.CallOption) (*GenerateIcalContentResponse, error)
}

type icalsClient struct {
	cc *grpc.ClientConn
}

func NewIcalsClient(cc *grpc.ClientConn) IcalsClient {
	return &icalsClient{cc}
}

func (c *icalsClient) GenerateIcalContent(ctx context.Context, in *GenerateIcalContentRequest, opts ...grpc.CallOption) (*GenerateIcalContentResponse, error) {
	out := new(GenerateIcalContentResponse)
	err := c.cc.Invoke(ctx, "/saastack.ical.v1.Icals/GenerateIcalContent", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// IcalsServer is the server API for Icals service.
type IcalsServer interface {
	// GenerateIcalContent generates ical content from id(can be employee or location id)
	GenerateIcalContent(context.Context, *GenerateIcalContentRequest) (*GenerateIcalContentResponse, error)
}

// UnimplementedIcalsServer can be embedded to have forward compatible implementations.
type UnimplementedIcalsServer struct {
}

func (*UnimplementedIcalsServer) GenerateIcalContent(ctx context.Context, req *GenerateIcalContentRequest) (*GenerateIcalContentResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GenerateIcalContent not implemented")
}

func RegisterIcalsServer(s *grpc.Server, srv IcalsServer) {
	s.RegisterService(&_Icals_serviceDesc, srv)
}

func _Icals_GenerateIcalContent_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GenerateIcalContentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IcalsServer).GenerateIcalContent(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.ical.v1.Icals/GenerateIcalContent",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IcalsServer).GenerateIcalContent(ctx, req.(*GenerateIcalContentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Icals_serviceDesc = grpc.ServiceDesc{
	ServiceName: "saastack.ical.v1.Icals",
	HandlerType: (*IcalsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GenerateIcalContent",
			Handler:    _Icals_GenerateIcalContent_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "ical.proto",
}
