// Code generated by protoc-gen-defaults. DO NOT EDIT.

package pb

import (
	"context"
	"database/sql"
	"fmt"

	rights "go.saastack.io/deployment/right"
	modulePB "go.saastack.io/modulerole/pb"
	rightspb "go.saastack.io/right/pb"
	"go.saastack.io/userinfo"
	"go.uber.org/fx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	ICALS_GENERATE_ICAL_CONTENT_ID = "/Icals/{id}**/.GenerateIcalContent"
)

var IcalsModuleName = "Icals"
var IcalsModulePattern = "/Icals/{parent}**/.*"

var IcalsResourcePaths = []*rightspb.RightsResource{

	{
		Name:        `GenerateIcalContent`,
		Description: ` GenerateIcalContent generates ical content from id(can be employee or location id)\n`,
		Resource:    `/Icals/{id}**/.GenerateIcalContent`,
	},
}

type rightsIcalsServer struct {
	IcalsSrv         IcalsServer
	moduleRoleServer modulePB.ModuleRoleServiceServer
	rightsCli        rightspb.RightValidatorsClient
	db               *sql.DB
}

func NewRightsIcalsServer(db *sql.DB,
	mrs modulePB.ModuleRoleServiceServer,
	c rightspb.RightValidatorsClient,

	config rights.ModuleRoleConfig,
	in struct {
		fx.In
		S IcalsServer `name:"public"`
	},
) struct {
	fx.Out
	S IcalsServer `name:"public"`
} {
	srv := &rightsIcalsServer{
		db:               db,
		rightsCli:        c,
		IcalsSrv:         in.S,
		moduleRoleServer: mrs,
	}

	if err := srv.RegisterModuleRoles(); err != nil {
		panic(err)
	}

	return struct {
		fx.Out
		S IcalsServer `name:"public"`
	}{
		S: srv,
	}
}

func (s *rightsIcalsServer) GenerateIcalContent(ctx context.Context, rightsvar *GenerateIcalContentRequest) (*GenerateIcalContentResponse, error) {

	ResourcePathOR := make([]string, 0)
	ResourcePathAND := make([]string, 0)

	ResourcePathOR = append(ResourcePathOR,

		fmt.Sprintf("/Icals/%s**/.GenerateIcalContent",

			rightsvar.GetId(),
		),
	)

	validations := map[string]bool{}

	res, err := s.rightsCli.IsValid(ctx, &rightspb.IsValidRequest{
		ResourcePathOr:       ResourcePathOR,
		ResourcePathAnd:      ResourcePathAND,
		UserId:               userinfo.FromContext(ctx).Id,
		ModuleName:           "Icals",
		AttributeValidations: validations,
		AllowParent:          false,
		AllowStaff:           false,
	})
	if err != nil {
		return nil, err
	}

	if !res.IsValid {
		return nil, status.Errorf(codes.PermissionDenied, res.Reason)
	}

	return s.IcalsSrv.GenerateIcalContent(ctx, rightsvar)
}

// function for constructor
func (s *rightsIcalsServer) RegisterModuleRoles() error {

	if _, err := s.moduleRoleServer.RegisterModuleRoleInMemory(context.Background(), &modulePB.ModuleRoleList{
		List: []*modulePB.ModuleRole{},
	}); err != nil {
		return err
	}

	return nil
}
