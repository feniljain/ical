package icals

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"go.appointy.com/waqt/ical-feed/pb"
	"go.saastack.io/deployment/http/router"
	"go.saastack.io/jaal/schemabuilder"
	"go.uber.org/fx"
	"google.golang.org/grpc"
)

// Module is the fx module encapsulating all the providers of the package
var Module = fx.Options(
	fx.Provide(

		NewIcalsServer,
		fx.Annotated{
			Name: "public",
			Target: func(srv pb.IcalsServer) pb.IcalsServer {
				return srv
			},
		},

		pb.NewLocalIcalsClient,
		fx.Annotated{
			Name: "public",
			Target: func(in struct {
				fx.In
				S pb.IcalsServer `name:"public"`
			}) pb.IcalsClient {
				return pb.NewLocalIcalsClient(in.S)
			},
		},

		fx.Annotated{
			Group:  "routes",
			Target: Routes,
		},

		fx.Annotated{
			Group:  "grpc-service",
			Target: RegisterGRPCService,
		},
		fx.Annotated{
			Group:  "graphql-service",
			Target: RegisterGraphQLService,
		},
		fx.Annotated{
			Group:  "http-service",
			Target: RegisterHttpService,
		},
	),
	fx.Decorate(
		pb.NewEventsIcalsServer,
	),
)

func RegisterGRPCService(in struct {
	fx.In
	Server pb.IcalsServer `name:"public"`
}) func(s *grpc.Server) {
	return func(s *grpc.Server) {
		pb.RegisterIcalsServer(s, in.Server)
	}
}

func RegisterGraphQLService(in struct {
	fx.In
	Client pb.IcalsClient `name:"public"`
}) func(s *schemabuilder.Schema) {
	return func(s *schemabuilder.Schema) {
		pb.RegisterIcalsOperations(s, in.Client)
	}
}

func RegisterHttpService(in struct {
	fx.In
	Client pb.IcalsClient `name:"public"`
}) func(*runtime.ServeMux, context.Context) error {
	return func(mux *runtime.ServeMux, ctx context.Context) error {
		return pb.RegisterIcalsHandlerClient(ctx, mux, in.Client)
	}
}

func Routes(in struct {
	fx.In
	Client pb.IcalsClient `name:"public"`
}) router.RouteFunc {
	return func(r router.Router) {
		r.Get("/export-ical", func(w http.ResponseWriter, r *http.Request) {

			//Extracting ID query
			id := r.URL.Query().Get("id")
			if len(id) < 3 {
				sendResponse(w, http.StatusBadRequest, nil, errors.New("Invalid ID"))
				return
			}

			//Generate ical content
			res, err := in.Client.GenerateIcalContent(context.Background(), &pb.GenerateIcalContentRequest{Id: id})
			if err != nil {
				sendResponse(w, http.StatusBadRequest, nil, err)
				return
			}

			//Creating temporary file
			file, err := os.CreateTemp("", strings.Replace(id, "/", "", -1))
			if err != nil {
				sendResponse(w, http.StatusBadRequest, nil, err)
				return
			}
			//Removing file
			defer os.Remove(file.Name())

			//Writing to file
			if _, err := file.Write([]byte(res.Content)); err != nil {
				sendResponse(w, http.StatusBadRequest, nil, err)
				return
			}

			//Extracting stats of a file
			fStat, _ := file.Stat()
			fileSize := strconv.FormatInt(fStat.Size(), 10)

			//Writing headers
			w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.ics", id))
			w.Header().Set("Content-Type", "text/calendar; charset=utf-8")
			w.Header().Set("Content-Length", fileSize)

			//Writing the file
			_, err = w.Write([]byte(res.Content))
			//_, err = io.Copy(w, file)
			if err != nil {
				sendResponse(w, http.StatusBadRequest, nil, err)
				return
			}

			//Close the file
			if err := file.Close(); err != nil {
				sendResponse(w, http.StatusBadRequest, nil, err)
			}

		})
	}
}

//Response depicts reponse structure to be sent
type Response struct {
	Data   map[string]interface{} `json:"data"`
	Errors []string               `json:"errors"`
}

func sendResponse(w http.ResponseWriter, statusCode int, data map[string]interface{}, e error) {
	resp := Response{
		Data:   nil,
		Errors: []string{e.Error()},
	}

	//Marshal JSON
	respJSON, err := json.Marshal(resp)
	if err != nil {
		sendResponse(w, http.StatusBadRequest, nil, errors.New("Internal Server Error"))
	}

	//Writing headers
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(respJSON)
}
