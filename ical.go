package icals

import (
	"context"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/anujdecoder/ics"
	"github.com/golang/protobuf/ptypes"
	appointmentpb "go.appointy.com/waqt/appointment/pb"
	"go.appointy.com/waqt/ical-feed/pb"
	employeepb "go.saastack.io/employee/pb"
	"go.saastack.io/idutil"
	locationpb "go.saastack.io/location/pb"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	locPrefix    = (&locationpb.Location{}).GetPrefix()
	empPrefix    = (&employeepb.Employee{}).GetPrefix()
	errInternal  = status.Error(codes.Internal, "oops! Something went wrong")
	errInvalidID = status.Error(codes.InvalidArgument, "Invalid ID")
)

type icalsServer struct {
	aCli appointmentpb.AppointmentsClient
	eCli employeepb.EmployeesClient
	lCli locationpb.LocationsClient
}

func NewIcalsServer(
	aCli appointmentpb.AppointmentsClient,
	eCli employeepb.EmployeesClient,
	lCli locationpb.LocationsClient,
) pb.IcalsServer {
	r := &icalsServer{
		aCli: aCli,
		eCli: eCli,
		lCli: lCli,
	}

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

// These functions are not implemented by CRUDGen, needed to be implemented

func appointmentsToICS(appointments []*appointmentpb.Appointment) (string, error) {
	eventsString := ""

	for _, appointment := range appointments {
		//Conversion of timestamps to needed format
		start, _ := ptypes.Timestamp(appointment.TimeSlot.StartTime)
		end, _ := ptypes.Timestamp(appointment.TimeSlot.StartTime)

		//Filling in basic structure of event for our ics content
		e := &ics.Event{
			Class:        ics.Classification_PUBLIC,
			Summary:      appointment.Service.Title,
			Status:       ics.EventStatus_CONFIRMED,
			DtStart:      start,
			DtEnd:        end,
			Transparency: ics.OPAQUE,
			//Organizer:    ics.Attendee{CommonName: m.OrganizerCn, EmailAddress: m.Or},
			UID: appointment.GetId(),
		}

		e.Description = "Description"

		//Changing event status according to appointment status
		if appointment.Status == appointmentpb.AppointmentStatus_APPOINTMENT_CANCELLED {
			e.Status = ics.EventStatus_CANCELLED
		}

		//Adding consumers/attendes of event
		for _, c := range appointment.Consumers {
			//Adding each attende's basic details
			at := ics.Attendee{
				EmailAddress: c.Email,
				Role:         ics.REQUIRED,
				PartStatus:   ics.AttendeeStatus_ACCEPTED,
				CuType:       ics.INDIVIDUAL,
			}

			//Adding data according to consumer type
			if data := c.ConsumerData.GetCustomerData(); data != nil {
				at.CommonName = fmt.Sprintf("%s %s", data.FirstName, data.LastName)
			} else if data := c.ConsumerData.GetStudentData(); data != nil {
				at.CommonName = fmt.Sprintf("%s %s", data.FirstName, data.LastName)
			} else {
				at.PartStatus = ics.AttendeeStatus_DECLINED
			}

			e.Attendees = append(e.Attendees, at)
		}

		showStaff := true
		//Adding the staff(s) which will be attending the appointment/event
		if appointment.Employee != nil && appointment.Employee.Id != "" && showStaff {
			e.Summary += " With " + appointment.Employee.FirstName
			sp := ics.Attendee{
				CommonName:   fmt.Sprintf("%s %s", appointment.Employee.FirstName, appointment.Employee.LastName),
				EmailAddress: appointment.Employee.Email,
				Role:         ics.REQUIRED,
				PartStatus:   ics.AttendeeStatus_ACCEPTED,
				CuType:       ics.INDIVIDUAL,
			}

			if appointment.Status == appointmentpb.AppointmentStatus_APPOINTMENT_CANCELLED {
				sp.PartStatus = ics.AttendeeStatus_DECLINED
			}

			e.Attendees = append(e.Attendees, sp)
		}

		//Converting to string
		eventString, err := e.Generate("prodID")
		if err != nil {
			return "", err
		}

		eventsString += eventString
	}

	return eventsString, nil
}

//GenerateIcalContent implements core BLoC for verification, getting appointments and generating ical content for the same.
func (s *icalsServer) GenerateIcalContent(ctx context.Context, in *pb.GenerateIcalContentRequest) (*pb.GenerateIcalContentResponse, error) {

	//Initial Validation of fields
	if err := in.Validate(); err != nil {
		return nil, err
	}

	//Initializing time for time-range to be used for getting appointments
	time7DaysAgo, err := ptypes.TimestampProto(time.Now().AddDate(0, 0, -7))
	if err != nil {
		return nil, err
	}

	endTime, err := ptypes.TimestampProto(time.Now().AddDate(1000, 0, 0))
	if err != nil {
		return nil, err
	}

	//Checking if ID is either emp ID or loc ID if not error is thrown
	if empPrefix == idutil.GetPrefix(in.GetId()) {

		//Verifying employee ID
		_, err := s.eCli.GetEmployee(ctx, &employeepb.GetEmployeeRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}})
		if err != nil {
			return nil, errInvalidID
		}

		//Fetching appointments corresponding to employee ID
		resp, err := s.aCli.ListAppointmentsForCalendar(ctx, &appointmentpb.ListAppointmentsForCalendarRequest{
			EmployeeIds: []string{in.GetId()},
			StartTime:   time7DaysAgo,
			EndTime:     endTime,
			Status:      appointmentpb.AppointmentStatus_APPOINTMENT_CONFIRMED,
			Timezone:    "Etc/UTC",
		})
		if err != nil {
			return nil, err
		}

		//Generating corresponding ics content
		events, err := appointmentsToICS(resp.GetAppointments())
		if err != nil {
			return nil, err
		}

		//Writing to ics file
		err = ioutil.WriteFile("icas-feed.ics", []byte(events), 0755)
		if err != nil {
			return nil, err
		}

		return &pb.GenerateIcalContentResponse{Content: events}, nil
	} else if locPrefix == idutil.GetPrefix(in.GetId()) {

		//Verifying location ID
		_, err := s.lCli.GetLocation(ctx, &locationpb.GetLocationRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}})
		if err != nil {
			return nil, errInvalidID
		}

		//Fetching appointments corresponding to location ID
		resp, err := s.aCli.ListAppointmentsForCalendar(ctx, &appointmentpb.ListAppointmentsForCalendarRequest{
			Parents:   []string{in.GetId()},
			StartTime: time7DaysAgo,
			EndTime:   endTime,
			Status:    appointmentpb.AppointmentStatus_APPOINTMENT_CONFIRMED,
			Timezone:  "Etc/UTC",
		})
		if err != nil {
			return nil, err
		}

		//Generating corresponding ics content
		events, err := appointmentsToICS(resp.GetAppointments())
		if err != nil {
			return nil, err
		}

		//Writing to ics file
		err = ioutil.WriteFile("icas-feed.ics", []byte(events), 0755)
		if err != nil {
			return nil, err
		}
		return &pb.GenerateIcalContentResponse{Content: events}, nil
	} else {
		return nil, errInvalidID
	}
}
